#include <crsRead/MCorsikaReader.h>

#include <crs/TSubBlock.h>
#include <crs/MRunHeader.h>
#include <crs/MEventHeader.h>
#include <crs/MEventEnd.h>
#include <crs/MParticleBlock.h>
#include <crs/MLongitudinalBlock.h>
#include <crs/MParticle.h>

#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>


#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <unistd.h>

std::string usage();
int getOptions(int argc, char** argv, int& n);



int main( int argc, char **argv )
{
  int shIndex = 1;
  const int nOptions = getOptions(argc, argv, shIndex);
  if(nOptions < 0 || argc-nOptions < 1 )
  {
    std::cout << usage() << std::endl;
    return 1;
  }

  /*
   * -----------------------------------------------
   * Reading the file
   * -----------------------------------------------
   */

  std::string fname(argv[nOptions]);

  crsRead::MCorsikaReader cr(fname, 1);
  crs::MRunHeader run;

  Double_t hs = 1.0;
  Int_t nBins = 1000;

  
  TH1D* hGround = new TH1D("hGround","hGround",nBins,0.0,hs);
  Int_t i=0;
  Double_t logE = 0.0;
  Double_t obsLev = 0.0;
  
  while( cr.GetRun( run ) ) // Loop over the runs. Usually one
  {
    crs::MEventHeader shower;
    while( cr.GetShower( shower ) )
    {
      i++;
      if(i != shIndex)
	continue;

      std::cout << "-----------------------------" << std::endl;
      std::cout << "plot of the shower nr: " << i << std::endl;
      logE = log10( shower.GetEnergy() );
      obsLev = shower.GetObservationHeight( shower.GetNObservationLevels()-1 )/100.0;
      std::cout << "shower obs level: " << obsLev << " m" << std::endl;
      
      crs::TSubBlock sub_block;
      while( cr.GetData( sub_block ) )
      {
	//std::cout << "Block type: " << data.GetBlockTypeName() << std::endl;

	crs::MParticleBlock data(sub_block);
	//double *pData = data.GetData();
	
	
	
	if( data.GetBlockType() == crs::TSubBlock::ePARTDATA )
	{
	  for( crs::MParticleBlock::ParticleListConstIterator p_it = data.FirstParticle();
	       p_it != data.LastParticle();
	       ++p_it )
	  {
	    crs::MParticle part(*p_it);

            double px = part.GetPx();
            double py = part.GetPy();
            double pz = part.GetPz();

            double p_tot = sqrt(px*px + py*py + pz*pz);
            //std::cout << "Total momentum: " << p_tot << std::endl;
            hGround->Fill(p_tot);
	  }
	}
	
	
      }
      break;
    }
  }

  if ( i < shIndex )
  {
    std::cout << "ERROR: The file contains " << i << " showers" << std::endl;
    return -1;
  }
  std::cout << "-----------------------------" << std::endl;

  TCanvas *cGround = new TCanvas("cFoot","cFoot",1600, 900);

  std::ostringstream title;
  title << "ground particle momentum for shower: " << shIndex;
  hGround->SetTitle( (title.str()).c_str() );
  hGround->SetFillColor(kBlue);
  hGround->SetFillStyle(3005);
  hGround->Draw();
  hGround->GetXaxis()->SetTitle("P [GeV/c]");

  cGround->cd()->SetLogy();
  
  std::ostringstream outFname;
  outFname << "groundMomenta_" << fname << "_" << shIndex << ".png";
  cGround->SaveAs(outFname.str().c_str());

  return 0;
}


std::string usage()
{
  std::string usg = "usage: ./energySpectra <corsika file name>";
  return usg;
}


int getOptions( int argc, char** argv, int &n )
{

  int c;
  while ( (c = getopt(argc, argv, "n:h")) != -1 )
  {
    switch(c)
    {
    case 'n':
      n = std::atoi(optarg);
      break;
      
    case 'h':
      return -2;

    default:
      return -2;
    }
  }
  return optind;
  
}
