#include <crsRead/MCorsikaReader.h>

#include <crs/TSubBlock.h>
#include <crs/MRunHeader.h>
#include <crs/MEventHeader.h>
#include <crs/MEventEnd.h>
#include <crs/MParticleBlock.h>
#include <crs/MLongitudinalBlock.h>
#include <crs/MParticle.h>

#include <TCanvas.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>


#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

#include <unistd.h>

std::string usage();
int getOptions(int argc, char** argv, int& n);



int main( int argc, char **argv )
{
  int shIndex = 1;
  const int nOptions = getOptions(argc, argv, shIndex);
  if(nOptions < 0 || argc-nOptions < 1 )
  {
    std::cout << usage() << std::endl;
    return 1;
  }

  /*
   * -----------------------------------------------
   * Reading the file
   * -----------------------------------------------
   */

  std::string fname(argv[nOptions]);

  crsRead::MCorsikaReader cr(fname, 1);
  crs::MRunHeader run;

  Double_t hs = 2000.0;
  Int_t nBins = hs;

  
  TH2D* xy_tot = new TH2D("xy_tot","xy_tot",nBins,-hs,hs,nBins,-hs,hs);
  TH2D* xy_e = new TH2D("xy_e","xy_e",nBins,-hs,hs,nBins,-hs,hs);
  TH2D* xy_c = new TH2D("xy_c","xy_c",nBins,-hs,hs,nBins,-hs,hs);
  TH2D* xy_m = new TH2D("xy_m","xy_m",nBins,-hs,hs,nBins,-hs,hs);
  TH2D* xy_n = new TH2D("xy_n","xy_n",nBins,-hs,hs,nBins,-hs,hs);
  Int_t i=0;
  Double_t logE = 0.0;
  Double_t obsLev = 0.0;
  
  while( cr.GetRun( run ) ) // Loop over the runs. Usually one
  {
    crs::MEventHeader shower;
    while( cr.GetShower( shower ) )
    {
      i++;
      if(i != shIndex)
	continue;

      std::cout << "-----------------------------" << std::endl;
      std::cout << "plot of the shower nr: " << i << std::endl;
      logE = log10( shower.GetEnergy() );
      obsLev = shower.GetObservationHeight( shower.GetNObservationLevels()-1 )/100.0;
      std::cout << "shower obs level: " << obsLev << " m" << std::endl;
      
      crs::TSubBlock sub_block;
      while( cr.GetData( sub_block ) )
      {
	//std::cout << "Block type: " << data.GetBlockTypeName() << std::endl;

	crs::MParticleBlock data(sub_block);
	//double *pData = data.GetData();
	
	
	
	if( data.GetBlockType() == crs::TSubBlock::ePARTDATA )
	{
	  for( crs::MParticleBlock::ParticleListConstIterator p_it = data.FirstParticle();
	       p_it != data.LastParticle();
	       ++p_it )
	  {
	    crs::MParticle part(*p_it);

	  Int_t pID = part.GetParticleID();

	    if( pID < 4 )
	      xy_e->Fill(part.GetX()/100.0, part.GetY()/100.0);

	    if( pID == 5 || pID == 6 )
	      xy_m->Fill(part.GetX()/100.0, part.GetY()/100.0);
	    
	    if( part.IsCherenkov() )
	      xy_c->Fill(part.GetX()/100.0, part.GetY()/100.0);
	    
	    if( part.IsNucleus() || ( pID >6 && pID <200 ) && !part.IsMuonProductionInfo())
	      xy_n->Fill(part.GetX()/100.0, part.GetY()/100.0);

            if( !part.IsMuonProductionInfo() )
              xy_tot->Fill(part.GetX()/100.0, part.GetY()/100.0);
	  }
	}
	
	
      }
      break;
    }
  }

  if ( i < shIndex )
  {
    std::cout << "ERROR: The file contains " << i << " showers" << std::endl;
    return -1;
  }
  std::cout << "-----------------------------" << std::endl;

  TCanvas *cFoot = new TCanvas("cFoot","cFoot",1600, 900);

  
  xy_tot->SetMarkerColor(kBlack);
  xy_n->SetMarkerColor(kRed);
  xy_c->SetMarkerColor(kViolet);
  xy_m->SetMarkerColor(kGreen);
  xy_e->SetMarkerColor(kBlue);

  xy_tot->SetMarkerStyle(2);
  xy_n->SetMarkerStyle(2);
  xy_c->SetMarkerStyle(2);
  xy_m->SetMarkerStyle(2);
  xy_e->SetMarkerStyle(2);


  std::ostringstream title;
  title << "log(E/[GeV]) = " << logE;
  xy_tot->SetTitle( (title.str()).c_str() );
  xy_tot->GetXaxis()->SetTitle( "X [m]" );
  xy_tot->GetYaxis()->SetTitle( "Y [m]" );
  
  xy_tot->Draw();  
  xy_e->Draw("SAME");
  xy_m->Draw("SAME");
  xy_n->Draw("SAME");
  xy_c->Draw("SAME");
  std::ostringstream outFname;
  outFname << "footprint_" << fname << "_" << shIndex << ".png";
  cFoot->SaveAs(outFname.str().c_str());

  return 0;
}


std::string usage()
{
  std::string usg = "usage: ./energySpectra <corsika file name>";
  return usg;
}


int getOptions( int argc, char** argv, int &n )
{

  int c;
  while ( (c = getopt(argc, argv, "n:h")) != -1 )
  {
    switch(c)
    {
    case 'n':
      n = std::atoi(optarg);
      break;
      
    case 'h':
      return -2;

    default:
      return -2;
    }
  }
  return optind;
  
}
