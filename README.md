# CORSIKA-Hands_on

exercises for the hands on session at the Malargue ISAPP school 2019 


**energySpectra**: plots the energy spectra of all the CORSIKA showers in a single file (energy range has to be corrected);

**angularDistribution**: plots the angular distribution of all the CORSIKA showers in a single file;

**footprint**: plots the distribution of particles on ground;

**dummySim**: performs a dummy simulation with a simple Detector schema.