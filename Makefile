##############################################################################################
# export coast dir
#--------------------------------------------------------------------------------------------

export COAST_DIR=/home/isapp/corsika/corsika-76900

#--------------------------------------------------------------------------------------------

LDFLAGS   = ${CXXFLAGS} -fPIC -ggdb3
LDFLAGS  += $(shell root-config --libs)
LDFLAGS  += -L$(COAST_DIR)/lib -L$(COAST_DIR)/lib/unknown
LDFLAGS  += -lCorsikaFileIO
LDFLAGS  += -lCorsikaIntern

CPPFLAGS  = ${CXXFLAGS} -c -fPIC -ggdb3 
CPPFLAGS += -I$(COAST_DIR)/include
CPPFLAGS += $(shell root-config --cflags)

SRCFILES  = $(wildcard *.cc)
OBJECTS   = $(patsubst %.cc, %.o, ${SCRFILES})

EXE       = energySpectra
EXE      += angularDistribution
EXE      += groundMomenta
EXE      += footprint
EXE      += dummySim
EXE      += longReader
all: ${EXE}

#--------------------------------------------------------------------------------------------

energySpectra:  energySpectra.o
		${CXX} $^ ${LDFLAGS} -o $@

angularDistribution: angularDistribution.o
		${CXX} $^ ${LDFLAGS} -o $@

groundMomenta:   groundMomenta.o
		${CXX} $^ ${LDFLAGS} -o $@

footprint:      footprint.o
		${CXX} $^ ${LDFLAGS} -o $@

dummySim:       dummySim.o dummyExperiment.o
		${CXX} $^ ${LDFLAGS} -o $@

longReader:     longReader.o
		${CXX} $^ ${LDFLAGS} -o $@

clean:
	@rm -f *.o *.so *.png
