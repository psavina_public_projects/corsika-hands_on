#include <crsRead/MCorsikaReader.h>
#include <crsRead/MLongfileReader.h>

#include <TCanvas.h>
#include <TGraph.h>
#include <iostream>
#include <string>
#include <sstream>

#include "TAxis.h"
#include "TGraph.h"
#include "TCanvas.h"

std::string usage();
int getOptions(int argc, char** argv, int& n);




int main(  int argc, char **argv )
{
  int shIndex = 1;
  const int nOptions = getOptions(argc, argv, shIndex);
  if(nOptions < 0 || argc-nOptions < 1 )
  {
    std::cout << usage() << std::endl;
    return 1;
  }

  /*
   * -----------------------------------------------
   * Reading the file
   * -----------------------------------------------
   */

  std::string fname(argv[nOptions]);

  crsRead::MLongfileReader lr(fname, 3);


  
  if(lr.ReadShower( shIndex ))
  {
    TGraph gLong( lr.GetNBinsEdep() -1 );
    TCanvas cLong("cLong","cLong", 1600, 900);

    for( int iDep = 0; iDep < lr.GetNBinsEdep() -1; iDep++)
    {
      
      const crsRead::MLongfileReader::EdepEntry& dEntry = lr.GetEntryEdep(iDep);
      gLong.SetPoint(iDep, dEntry.X, dEntry.dEdXsum);
      
    }
    gLong.SetTitle(" longitudinal development ");
    gLong.GetXaxis()->SetTitle("X [gr/cm^{2}]");
    gLong.GetYaxis()->SetTitle("dE/dX [GeV/(gr/cm^{2})]");
    gLong.SetMarkerStyle(20);
    gLong.SetMarkerColor(kBlue);
    gLong.Draw("AP");
      
    std::ostringstream outFname;
    outFname << "long_" << fname << "_" << shIndex << ".png";
    cLong.SaveAs(outFname.str().c_str());
  }
}


std::string usage()
{
  std::string usg = "usage: ./longReader -n <#shower> <corsika file name>";
  return usg;
}


int getOptions( int argc, char** argv, int &n )
{

  int c;
  while ( (c = getopt(argc, argv, "n:h")) != -1 )
  {
    switch(c)
    {
    case 'n':
      n = std::atoi(optarg);
      break;
      
    case 'h':
      return -2;

    default:
      return -2;
    }
  }
  return optind;
  
}

